FROM osgeo/gdal:alpine-small-3.5.2 as builder

WORKDIR /geoserver-service

COPY . .

RUN apk update
RUN apk add pkgconfig go
